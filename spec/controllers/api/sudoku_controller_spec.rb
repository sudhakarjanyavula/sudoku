require 'rails_helper'
RSpec.describe Api::SudokuController do
	let(:solvable_data) {[[5,4,nil,nil,2,nil,8,nil,6],
[nil,1,9,nil,nil,7,nil,nil,3],
[nil,nil,nil,3,nil,nil,2,1,nil],
[9,nil,nil,4,nil,5,nil,2,nil],
[nil,nil,1,nil,nil,nil,6,nil,4],
[6,nil,4,nil,3,2,nil,8,nil],
[nil,6,nil,nil,nil,nil,1,9,nil],
[4,nil,2,nil,nil,9,nil,nil,5],
[nil,9,nil,nil,7,nil,4,nil,2]
].to_s}

	let(:unsolvable_data) {[[9,4,nil,nil,2,nil,8,nil,6],
[nil,1,9,nil,nil,7,nil,nil,3],
[nil,nil,nil,3,nil,nil,2,1,nil],
[9,nil,nil,4,nil,5,nil,2,nil],
[nil,nil,1,nil,nil,nil,6,nil,4],
[6,nil,4,nil,3,2,nil,8,nil],
[nil,6,nil,nil,nil,nil,1,9,nil],
[4,nil,2,nil,nil,9,nil,nil,5],
[nil,9,nil,nil,7,nil,4,nil,2]
].to_s}
		let (:solved_sudoku) {[
		[5,4,3,9,2,1,8,7,6],
		[2,1,9,6,8,7,5,4,3],
		[8,7,6,3,5,4,2,1,9],
		[9,8,7,4,6,5,3,2,1],
		[3,2,1,7,9,8,6,5,4],
		[6,5,4,1,3,2,9,8,7],
		[7,6,5,2,4,3,1,9,8],
		[4,3,2,8,1,9,7,6,5],
		[1,9,8,5,7,6,4,3,2]]}

	describe "Sudoku" do
		it 'Api success Response' do 
			post :create, params: {data: solvable_data}
			expect(response.status).to eq(200)
		end
		it 'Sudoku Solution' do
			post :create, params: {data: solvable_data}
			body = JSON.parse(response.body)
			expect(body["solution"]).to eq(solved_sudoku)
		end
		it 'unsolvable Sudoku' do
			post :create, params: {data: unsolvable_data}
			body = JSON.parse(response.body)
			expect(body["error"]).to eq(["not a valid sudoku"])
		end
	end
end